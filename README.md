# Konkurs Matematyczny o Puchar Dyrektora V Liceum Ogólnokształcącego w Bielsku-Białej

![Podgląd strony](static/screenshot.png)

## Pomoc ze stroną

Kod jest dostępny publicznie w celu ulepszania strony. Jeśli chcesz wprowadzić nową funkcję lub poprawkę, jestem otwarta na zmiany.

## Instalacja lokalna

### Sklonowanie

```bash
$ git clone https://gitlab.com/yaemiku/puchar
```

### Konfiguracja

#### Plik `.env`:

```conf
DEBUG=0 / 1
SECRET_KEY=...
EMAIL_HOST=...
EMAIL_PORT=...
EMAIL_HOST_USER=...
EMAIL_HOST_PASSWORD=...

DB_USER=...
DB_PASSWORD=...
```

#### Potem:

```bash
$ python manage.py migrate
$ python manage.py runserver
```
