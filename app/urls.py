from django.urls import path
from django.views.generic.base import RedirectView

from .views import SignUpView, SubmitStudentsView, UpdateAccountView, AboutContestView, NewsView, ScoresView, RulesView, TestsView, ContactView, RobotsView, SitemapView

urlpatterns = [
    path('', NewsView.as_view(), name='news'),
    path('aktualnosci/', RedirectView.as_view(url='/')),
    path('konkurs/', AboutContestView.as_view(), name='aboutcontest'),
    path('regulamin/', RulesView.as_view(), name='rules'),
    path('zadania/', TestsView.as_view(), name='tests'),
    path('wyniki/', ScoresView, name='scores'),
    path('wyniki/<int:year>/', ScoresView, name='scores'),
    path('kontakt/', ContactView.as_view(), name='contact'),
    path('rejestracja/', SignUpView.as_view(), name='signup'),

    path('konto/', UpdateAccountView, name='account'),
    path('zgloszenie/', SubmitStudentsView, name='submission'),

    path('robots.txt', RobotsView.as_view()),
    path('sitemap.xml', SitemapView.as_view())
]
