from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import School, Student


class StudentForm(forms.ModelForm):
    field_order = ('name', 'surname', 'grade')

    class Meta:
        model = Student
        fields = ('name', 'surname', 'grade')

    def __init__(self, *arg, **kwarg):
        super(StudentForm, self).__init__(*arg, **kwarg)
        self.empty_permitted = True


class CustomUserCreationForm(UserCreationForm):
    email = forms.EmailField(
        max_length=254,
        required=True,
        label='Adres email',
        help_text='Prosimy o podanie adresu mailowego nauczyciela zgłaszającego drużynę, nie szkoły'
    )
    phone = forms.IntegerField(
        required=True,
        label='Numer telefonu',
        help_text='Prosimy o podanie numeru telefonu nauczyciela zgłaszającego drużynę, nie szkoły'
    )
    password1 = forms.CharField(label='Hasło', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Zatwierdź hasło',
        help_text='Wpisz wybrane hasło jeszcze raz, dla weryfikacji',
        widget=forms.PasswordInput
    )

    class Meta:
        model = School
        fields = ('email', 'phone', 'password1', 'password2')


class CustomUserChangeForm(UserChangeForm):
    password = None

    name = forms.CharField(required=True, max_length=250, label='Nazwa szkoły')
    town = forms.CharField(required=True, max_length=250, label='Miejscowość')
    address = forms.CharField(required=True, max_length=250, label='Adres')

    class Meta:
        model = School
        fields = ('name', 'town', 'address')
